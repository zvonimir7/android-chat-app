package com.ferit.kvchatappzh.Model;

public class User {

    private String id;
    private String username;
    private String imageURL;
    private String status;
    private String password;

    public User(String id, String username, String imageURL, String status, String password) {
        this.id = id;
        this.username = username;
        this.imageURL = imageURL;
        this.status = status;
        this.password = password;
    }

    public User() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }
}
